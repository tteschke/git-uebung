package test;

/**
 * Ein Studi hat was dokumentiert!
 * Ein weiterer Kommentar wurde hinzugefügt
 */
public class TestClass {

	public String neandereMethode() {
		// Das soll einer verstehen ;-)
		return "Hallo, " + "Welt" + "Test Hallo Hallo"+ "!";
	}

	public void methodeZweiHundert(int anzahl) {
		for (int i = 0; i < anzahl; i++) {
			System.out.println("Eine Ausgabe: " + i);
		}
	}

	public void methodeDreiEinhalb() {
		// Kann die nicht weg?
		
		// obsolete
		// System.out.println("5");
		System.out.println("42");
	}


	// Kommentar: finde ich gut
	public void methodeSechs1() {
		System.out.println("die vier ist auch cool.");

		// Demo-Feature:
		System.out.println("DEMOO schreibt man mit zwei O");
		// Demo 2:
		methodeZwei(2);
	}

	public void methodeVierzehn() {
		// Feature overtime complete
		System.out.println("Methode gibt aus: ");
		System.out.println("Hallo!");
	}

	public void halloTest(String adjektiv) {
		System.out.println("Das");
		System.out.println("ist");
		System.out.println("ein " + adjektiv + " Test");
	}
}
